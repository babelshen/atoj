export interface ICreateStatements {
  court: string;
  judge: string;
  numberOfCase: string;
  name: string;
  numberOfTelephone: string;
  mail: string;
  date: string;
  time: string;
  document: string;
  [propName: string]: string;
}

export interface IInfo {
  id: number;
  title: string;
  titleStatement: string;
  reason: string;
  law?: string[];
  noLaw?: boolean;
  based: string;
  ask: string[];
  video?: string;
  defaultVariables: {
    name: string;
    label: string;
    type: string;
    required: boolean;
  }[];
}

export interface IFieldsInfo {
  name: string;
  label: string;
  type: string;
  required: boolean;
}

export interface IDate {
  year: number;
  month: number;
  day: number;
}
