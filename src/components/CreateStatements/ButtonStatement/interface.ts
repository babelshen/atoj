import { ICreateStatements } from '../interface';

export interface IButtonStatement {
  state: ICreateStatements;
  sign: string;
  setSign: (param: string) => void;
  setOpenSign: (param: boolean) => void;
}
