import GeneralButton from 'components/UI/GeneralButton';
import { IButtonStatement } from './interface';
import style from './ButtonStatement.module.scss';

const ButtonStatement: React.FC<IButtonStatement> = ({ state, sign, setSign, setOpenSign }) => (
  <div className={style.statement__buttons}>
    {state && !sign ? (
      <GeneralButton type="button" onClick={() => setOpenSign(true)}>
        Додати графічний підпис
      </GeneralButton>
    ) : (
      false
    )}

    {sign ? (
      <GeneralButton onClick={() => setSign('')} type="button">
        Видалити графічний підпис
      </GeneralButton>
    ) : (
      false
    )}
  </div>
);

export default ButtonStatement;
