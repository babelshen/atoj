import { ICreateStatements, IInfo } from '../interface';

export type NameType = 'time' | 'date' | 'court' | 'judge' | 'numberOfCase' | 'name' | 'numberOfTelephone' | 'mail' | 'document';

export interface IFormStatement {
  setState: (param: ICreateStatements | null) => void;
  info: IInfo;
  setInfo: (param: IInfo | null) => void;
  setFirstStep: (param: boolean) => void;
  setSecondStep: (param: boolean) => void;
}
