import { SubmitHandler, useForm } from 'react-hook-form';
import GeneralButton from 'components/UI/GeneralButton';
import { ICreateStatements } from '../interface';
import { IFormStatement, NameType } from './interface';
import style from './FormStatement.module.scss';

const FormStatement: React.FC<IFormStatement> = ({ setState, info, setInfo, setFirstStep, setSecondStep }) => {
  const { register, handleSubmit, reset } = useForm<ICreateStatements>({ mode: 'onBlur', shouldUseNativeValidation: true });

  const onSubmit: SubmitHandler<ICreateStatements> = (data) => setState(data);

  const handleClickChangeCategory = () => {
    setInfo(null);
    setFirstStep(true);
    setState(null);
    reset();
  };

  const handleClickChangeTemplate = () => {
    setInfo(null);
    setSecondStep(true);
    setState(null);
    reset();
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)} className={style.form}>
      <div className={style.form__wrapper}>
        {info.defaultVariables.map((item) => (
          <div key={item.name} className={style.form__div}>
            <input type={item.type} className={style.form__input} placeholder=" " {...register(item.name as NameType, { required: item.required })} />
            <label className={style.form__label} htmlFor={item.name}>
              {item.label}
            </label>
          </div>
        ))}
      </div>

      <div className={style.statement__buttons}>
        <GeneralButton type="button" onClick={handleClickChangeCategory}>
          Обрати іншу категорію справи
        </GeneralButton>

        <GeneralButton type="button" onClick={handleClickChangeTemplate}>
          Обрати інший шаблон
        </GeneralButton>
        <GeneralButton type="submit" onClick={handleSubmit(onSubmit)}>
          Сформувати звернення
        </GeneralButton>
      </div>
    </form>
  );
};

export default FormStatement;
