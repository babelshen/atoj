import { RefObject } from 'react';
import { ReactSketchCanvasRef } from 'react-sketch-canvas';

export interface IModalWindow {
  canvasRef: RefObject<ReactSketchCanvasRef>;
  setSign: (param: string) => void;
  setOpenSign: (param: boolean) => void;
}
