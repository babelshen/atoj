import { ReactSketchCanvas } from 'react-sketch-canvas';
import { IModalWindow } from './interface';
import style from './ModalWindow.module.scss';

const ModalWindow: React.FC<IModalWindow> = ({ canvasRef, setSign, setOpenSign }) => (
  <div className={style.modal}>
    <div className={style.modal__content}>
      <ReactSketchCanvas width="50vw" height="50vh" ref={canvasRef} strokeWidth={5} strokeColor="black" />
      <div className={style.buttons}>
        <button
          type="button"
          className={style.button}
          onClick={() => {
            setOpenSign(false);
            canvasRef.current
              ? canvasRef.current.exportImage('png').then((data) => {
                  setSign(data);
                })
              : false;
          }}
        >
          Вставити
        </button>

        <button
          className={style.button}
          type="button"
          onClick={() => {
            canvasRef.current ? canvasRef.current.clearCanvas() : false;
          }}
        >
          Видалити
        </button>
        <button className={style.button} type="button" onClick={() => setOpenSign(false)}>
          Закрити
        </button>
      </div>
    </div>
  </div>
);

export default ModalWindow;
