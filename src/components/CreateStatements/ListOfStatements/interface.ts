import { IInfo } from '../interface';

export interface IListOfStatements {
  category: string;
  setCategory: (param: string) => void;
  setInfo: (param: IInfo) => void;
  setFirstStep: (param: boolean) => void;
  setSecondStep: (param: boolean) => void;
}
