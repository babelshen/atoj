import GeneralButton from 'components/UI/GeneralButton';
import CivilStatements from './CivilStatements';
import { IListOfStatements } from './interface';
import style from './ListOfStatements.module.scss';

const ListOfStatements: React.FC<IListOfStatements> = ({ category, setCategory, setInfo, setFirstStep, setSecondStep }) => {
  const handleClick = () => {
    setCategory('');
    setFirstStep(true);
  };
  if (category === 'Civil') {
    return (
      <article className={style.list_statements__wrapper}>
        <CivilStatements setInfo={setInfo} setSecondStep={setSecondStep} />
        <GeneralButton type="button" onClick={handleClick}>
          Обрати іншу категорію справи
        </GeneralButton>
      </article>
    );
  }
  if (category === 'Criminal') {
    return (
      <article className={style.list_statements__wrapper}>
        <p className={style.not_found_info}>Поки що немає даних</p>
        <GeneralButton type="button" onClick={handleClick}>
          Обрати іншу категорію справи
        </GeneralButton>
      </article>
    );
  }
  if (category === 'Offenses') {
    return (
      <article className={style.list_statements__wrapper}>
        <p className={style.not_found_info}>Поки що немає даних</p>
        <GeneralButton type="button" onClick={handleClick}>
          Обрати іншу категорію справи
        </GeneralButton>
      </article>
    );
  }
};

export default ListOfStatements;
