/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import { civilStatements } from 'constants/statements';
import { IInfo } from 'components/CreateStatements/interface';
import { ICivilStatements } from './interface';
import style from './CivilStatements.module.scss';

const CivilStatements: React.FC<ICivilStatements> = ({ setInfo, setSecondStep }) => (
  <ul>
    {civilStatements.map((item) => (
      <li key={item.id} className={style.list_statements__item}>
        <span
          onClick={() => {
            setInfo(item as IInfo);
            setSecondStep(false);
          }}
        >
          {item.title}
        </span>
      </li>
    ))}
  </ul>
);
export default CivilStatements;
