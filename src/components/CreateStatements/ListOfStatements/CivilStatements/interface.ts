import { IInfo } from 'components/CreateStatements/interface';

export interface ICivilStatements {
  setInfo: (param: IInfo) => void;
  setSecondStep: (param: boolean) => void;
}
