import { Document, Page, Text, View, Font, Image, PDFViewer } from '@react-pdf/renderer';
import fontOpenSansBold from './fonts/OpenSans-Bold.ttf';
import fontOpenSansSemiBold from './fonts/OpenSans-SemiBold.ttf';
import fontOpenSansRegular from './fonts/OpenSans-Regular.ttf';
import { ICreateStatements, IDate, IInfo } from '../interface';
import style from './Statements.module.scss';
import { styles } from './Statement.style';

const Statement: React.FC<{ state: ICreateStatements; info: IInfo; dateNow: IDate; sign: string }> = ({ state, info, dateNow, sign }) => {
  Font.register({
    family: 'Open Sans',
    fonts: [
      { src: fontOpenSansRegular }, 
      { src: fontOpenSansBold, fontWeight: 700 }, 
      { src: fontOpenSansSemiBold, fontWeight: 600 }],
  });

  return (
    <PDFViewer width="100%" height="500px">
      <Document>
        <Page size="A4" style={styles.page}>
          <View style={styles.header}>
            <Text>Cудді {state.court}</Text>
            <Text>{state.judge}</Text>
            <Text>{state.name}</Text>

            {state.numberOfTelephone ? <Text>Телефон: {state.numberOfTelephone}</Text> : false}

            {state.numberOfTel ? <Text>Телефон: {state.numberOfTel}</Text> : false}

            {state.mail ? <Text>E-mail: {state.mail}</Text> : false}

            {state.email ? <Text>E-mail: {state.email}</Text> : false}

            {state.numberOfCase ? <Text>Справа № {state.numberOfCase}</Text> : false}
          </View>

          <View style={styles.title}>
            <Text>{info.titleStatement}</Text>
          </View>

          <View style={styles.body}>
            <Text>
              {state.dateOfJudgment || state.money || info.noLaw ? false : `У провадженні ${state.court} перебуває на розгляді справа`}

              {state.numberOfCase ? ` № ${state.numberOfCase}.` : `, у якій я зазначений як учасник.`}
            </Text>

            <Text>
              {state.date || state.time ? `Розгляд справи призначено` : false}

              {state.date ? `на ${state.date}` : false}
              {state.time ? ` о ${state.time}` : false}
            </Text>

            <Text>
              {state.dateOfJudgment ? `${state.dateOfJudgment}` : false}

              {info.reason}

              {state.money ? `${state.money} грн` : false}

              {state.processReason ? `, оскільки ${state.processReason}` : false}

              {state.reason ? state.reason : false}

              {state.email ? `${state.email} ` : false}

              {state.anotherCaseCategory || state.anotherCaseNumber || state.anotherCourt ? ` - ` : false}

              {state.anotherCaseCategory ? state.anotherCaseCategory : false}

              {state.anotherCaseNumber ? ` № ${state.anotherCaseNumber}` : false}

              {state.anotherCourt ? ` призначена ${state.anotherCourt}. ` : false}

              {state.reasonOfSkipping ? `${state.reasonOfSkipping}.` : false}

              {state.numberOfTel ? `${state.numberOfTel}.` : false}
            </Text>
          </View>
          <View style={styles.body}>
            {info.law
              ? info.law.map((item, index) => (
                  <Text key={index}>
                    {item}
                    {state.reasonRefund ? ` в разі ${state.reasonRefund}.` : false}
                  </Text>
                ))
              : false}

            {info.video ? (
              <Text>
                {info.video} {state.videoCourt}
              </Text>
            ) : (
              false
            )}

            {info.based ? <Text>{info.based}</Text> : false}
          </View>
          <View style={styles.ask}>{info.ask ? <Text>ПРОШУ СУД:</Text> : false}</View>
          <View style={styles.body}>{info.ask ? info.ask.map((item, index) => <Text key={index}>{item}</Text>) : false}</View>
          <View style={styles.body}>
            {state.document ? (
              <>
                <Text>До заяви додаю:</Text>
                {state.document.split(';').map((item, index) => (
                  <Text key={index}>{item}</Text>
                ))}
              </>
            ) : (
              false
            )}
          </View>
          <View style={styles.footer}>
            <Text>{`${dateNow.day < 10 ? `0${dateNow.day}` : dateNow.day}.${dateNow.month}.${dateNow.year}`}</Text>

            {sign ? (
              <>
                <Image style={styles.image} source={{ uri: sign, method: 'GET', headers: {}, body: '' }} />
                <div className={style.signature__wrapper}>
                  <img className={style.signature} src={sign} alt="Signature" title="Ваш підпис" />
                </div>
              </>
            ) : (
              false
            )}
          </View>
        </Page>
      </Document>
    </PDFViewer>
  );
};

export default Statement;
