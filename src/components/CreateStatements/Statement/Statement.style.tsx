import { StyleSheet } from '@react-pdf/renderer';

export const styles = StyleSheet.create({
  page: {
    display: 'flex',
    flexDirection: 'column',
    backgroundColor: 'white',
    fontFamily: 'Open Sans',
    fontSize: '14px',
    padding: '10px 40px 10px 80px',
    gap: '10px',
  },

  header: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-end',
    textAlign: 'right',
    fontFamily: 'Open Sans',
    gap: '10px',
  },

  title: {
    textAlign: 'center',
    fontSize: '16px',
    margin: '20px 0',
    fontFamily: 'Open Sans',
    fontWeight: 700,
  },

  body: {
    display: 'flex',
    flexDirection: 'column',
    gap: '5px',
    lineHeight: '150%',
    position: 'relative',
    textAlign: 'justify',
  },

  ask: {
    textAlign: 'center',
    margin: '20px 0',
    fontFamily: 'Open Sans',
    fontWeight: 700,
  },

  footer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginRight: '100px',
  },

  image: {
    maxWidth: '150px',
    maxHeight: '150px',
  },
});
