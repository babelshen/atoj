export interface IChooseCaseCategory {
  setCategory: (param: string) => void;
  setFirstStep: (param: boolean) => void;
  setSecondStep: (param: boolean) => void;
}
