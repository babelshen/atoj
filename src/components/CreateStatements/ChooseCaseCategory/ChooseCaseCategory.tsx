import { caseCategoryButton } from 'constants/caseCategoryButton';
import CustomButton from 'components/UI/CustomButton';
import { IChooseCaseCategory } from './interface';
import style from './ChooseCaseCategory.module.scss';

const ChooseCaseCategory: React.FC<IChooseCaseCategory> = ({ setCategory, setFirstStep, setSecondStep }) => (
  <>
    <h3 className={style.step__description}>Оберіть категорію справи</h3>
    <div className={style.category__button__wrapper}>
      {caseCategoryButton.map((item) => (
        <CustomButton
          key={item.id}
          onClick={() => {
            setCategory(item.category);
            setFirstStep(false);
            setSecondStep(true);
          }}
        >
          {item.name}
        </CustomButton>
      ))}
    </div>
  </>
);

export default ChooseCaseCategory;
