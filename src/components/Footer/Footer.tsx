import { memo } from 'react';
import { Link } from 'react-router-dom';
import { mainPageLink, project } from 'constants/globalVariables';
import { instructionsLinks, projectLinks } from 'constants/footerLinks';
import style from './Footer.module.scss';

const Footer = () => (
  <footer className={style.footer}>
    <div className={style.footer__main_info}>
      <Link to={mainPageLink}>
        <h2 className={style.logo}>{project}</h2>
      </Link>
      <p className={style.footer__official__info}>Уся інформація про суди, що використовується на сайті, отримана з відкритих джерел офіційного сайту судової влади України - court.gov.ua</p>
      <p className={style.footer__official__info}>Розпорядником інформації є Державна судова адміністрація України</p>
    </div>
    <div className={style.footer__additional_info}>
      <div className={style.footer__project}>
        <h3 className={style.footer__title}>Проєкт</h3>
        {projectLinks.map((item) => (
          <Link to={item.address} key={item.id}>
            {item.name}
          </Link>
        ))}
      </div>
      <div className={style.footer__instructions}>
        <h3 className={style.footer__title}>Інструкції</h3>
        {instructionsLinks.map((item) => (
          <Link to={item.address} key={item.id}>
            {item.name}
          </Link>
        ))}
      </div>
    </div>
  </footer>
);

export default memo(Footer);
