import { memo, useState } from 'react';
import { Link } from 'react-router-dom';
import { mainPageLink, project } from 'constants/globalVariables';
import MobileMenu from 'components/MobileMenu';
import Menu from '../Menu';
import style from './Header.module.scss';

const Header = () => {
  const [scroll, setScroll] = useState<boolean>(true);

  return (
    <header className={style.header}>
      <Link to={mainPageLink}>
        <h1 className={style.logo}>{project}</h1>
      </Link>
      {window.innerWidth > 590 ? <Menu /> : <MobileMenu scroll={scroll} setScroll={setScroll} />}
    </header>
  );
};

export default memo(Header);
