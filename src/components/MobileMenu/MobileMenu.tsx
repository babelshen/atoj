import { useRef } from 'react';
import { Link } from 'react-router-dom';
import { links } from 'constants/menuLinks';
import { IMobileMenu } from './interface';
import style from './MobileMenu.module.scss';

const MobileMenu: React.FC<IMobileMenu> = ({ scroll, setScroll }) => {
  const overlayRef = useRef<HTMLDivElement | null>(null);
  const buttonRef = useRef<HTMLButtonElement | null>(null);

  const handleMobileClickMenu = () => {
    const overlayElement = overlayRef.current;
    const buttonElement = buttonRef.current;
    if (buttonElement) {
      buttonElement.classList.toggle(style.active);
    }
    if (overlayElement) {
      overlayElement.classList.toggle(style.open);
    }
    setScroll(!scroll);
    if (scroll) {
      document.body.style.overflow = 'hidden';
    } else {
      document.body.style.overflow = 'auto';
    }
  };

  const handleLinkClick = () => {
    const overlayElement = overlayRef.current;
    const buttonElement = buttonRef.current;

    if (buttonElement) {
      buttonElement.classList.toggle(style.active);
    }

    if (overlayElement) {
      overlayElement.classList.remove(style.open);
    }
    setScroll(false);
    document.body.style.overflow = 'auto';
  };

  return (
    <>
      <button type="button" className={style.nav__burger_wrapper} onClick={handleMobileClickMenu} ref={buttonRef}>
        <div className={style.nav__burger_first} />
        <div className={style.nav__burger_second} />
        <div className={style.nav__burger_third} />
      </button>
      <div className={style.overlay} ref={overlayRef}>
        <nav className={style.mobile_menu}>
          <ul>
            {links.map((item) => (
              <li key={item.id}>
                <Link to={item.address} className={style.mob_nav__link} onClick={handleLinkClick}>
                  {item.name}
                </Link>
              </li>
            ))}
          </ul>
        </nav>
      </div>
    </>
  );
};

export default MobileMenu;
