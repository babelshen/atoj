export interface IMobileMenu {
  scroll: boolean;
  setScroll: (param: boolean) => void;
}
