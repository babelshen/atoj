import { ICustomButton } from './interface';
import style from './CustomButton.module.scss';

const CustomButton: React.FC<ICustomButton> = ({ children, onClick }) => (
  <button type="button" className={style.button} onClick={onClick}>
    <span className={style.button__text}>{children}</span>
  </button>
);

export default CustomButton;
