import { MouseEventHandler, ReactNode } from 'react';

export interface ICustomButton {
  children: ReactNode;
  onClick: MouseEventHandler<HTMLButtonElement>;
}
