import React from 'react';
import { FieldError } from 'react-hook-form';
import style from './CustomInput.module.scss';

const CustomInput = React.forwardRef(
  (
    { type, id, label, placeholder, error, ...props }: { type: string; id: string; label: string; placeholder: string; error?: FieldError },
    ref: React.Ref<HTMLInputElement>
  ) => (
      <label htmlFor={id} className={style.label}>
        {label}
        <input
            className={style.input}
            type={type}
            placeholder={placeholder}
            id={id}
            ref={ref}
            {...props}
        />
        {error ? <p className={style.error_message__text}>{error.message}</p> : false}
      </label>
    )
);

export default CustomInput;
