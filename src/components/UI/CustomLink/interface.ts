import { ReactNode } from 'react';

export interface ICustomLink {
  children: ReactNode;
  link: string;
  disabled?: boolean;
}
