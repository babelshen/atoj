import { Link } from 'react-router-dom';
import { ICustomLink } from './interface';
import style from './CustomLink.module.scss';

const CustomLink: React.FC<ICustomLink> = ({ children, link, disabled = false }) => {
  if (disabled) {
    return <div className={style.disabled__link}>{children}</div>;
  }
  return (
    <Link className={style.link} to={link}>
      {children}
    </Link>
  );
};

export default CustomLink;
