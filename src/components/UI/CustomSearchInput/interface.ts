import { ChangeEventHandler } from 'react';

export interface ICustomSearchInput {
  type: string;
  placeholder: string;
  onChange: ChangeEventHandler<HTMLInputElement>;
  value: string;
  onClick: () => void;
}
