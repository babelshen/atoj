import { ICustomSearchInput } from './interface';
import cross from '/cross.png';
import style from './CustomSearchInput.module.scss';

const CustomSearchInput: React.FC<ICustomSearchInput> = ({ type, placeholder, onChange, value, onClick }) => (
  <div className={style.wrapper__input}>
    <input type={type} placeholder={placeholder} onChange={onChange} value={value} className={style.input} />
    <button type="button" className={style.cross__wrapper} onClick={onClick}>
      <img className={style.cross} src={cross} alt="Delete search" title="Видалити пошуковий запит" />
    </button>
  </div>
);

export default CustomSearchInput;
