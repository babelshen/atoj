import style from './CustomLoading.module.scss';

const CustomLoading = () => (
  <div className={style.loading__wrapper}>
    <img className={style.loading} src="https://i16.servimg.com/u/f16/20/20/43/41/spinne10.gif" alt="Завантаження даних" title="Завантаження даних" />
  </div>
);

export default CustomLoading;
