import React from "react";
import { FieldError } from "react-hook-form";
import style from './CustomTextarea.module.scss';

const CustomTextarea = React.forwardRef(
    (
      { rows, id, label, error, ...props }: { rows: number; id: string; label: string; error?: FieldError },
      ref: React.Ref<HTMLTextAreaElement>
    ) => (
        <label htmlFor={id} className={style.label}>
          <span className={style.label__text}>{label}</span>
          <textarea
              className={style.textarea}
              rows={rows}
              id={id}
              ref={ref}
              {...props}
          />
          {error ? <p className={style.error_message__text}>{error.message}</p> : false}
        </label>
      )
  );

    export default CustomTextarea;