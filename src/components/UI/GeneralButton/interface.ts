import { ReactNode } from 'react';

export interface IGeneralButton {
  children: ReactNode;
  type: 'button' | 'submit' | 'reset' | undefined;
  onClick: () => void;
}
