import { IGeneralButton } from './interface';
import style from './GeneralButton.module.scss';

const GeneralButton: React.FC<IGeneralButton> = ({ children, type, onClick }) => (
  // eslint-disable-next-line react/button-has-type
  <button type={type} className={style.button} onClick={onClick}>
    {children}
  </button>
);

export default GeneralButton;
