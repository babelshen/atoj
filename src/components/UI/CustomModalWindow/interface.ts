import { ReactNode } from "react";

export interface ICustomModalWindow {
    children: ReactNode;
    onClick: () => void;
}