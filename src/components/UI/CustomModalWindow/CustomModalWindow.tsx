import GeneralButton from '../GeneralButton';
import style from './CustomModalWindow.module.scss';
import { ICustomModalWindow } from './interface';

const CustomModalWindow: React.FC<ICustomModalWindow> = ({children, onClick}) => (
    <div className={style.modal}>
        <div className={style.modal__content}>
            {children}
            <GeneralButton type='button' onClick={onClick}>
                Закрити
            </GeneralButton>
        </div>
    </div>
)

export default CustomModalWindow;