import { ReactNode } from 'react';
import warn from '/warning.png';
import style from './WarningText.module.scss';

const WarningText: React.FC<{ children: ReactNode }> = ({ children }) => (
  <div className={style.text__wrapper}>
    <div className={style.image__wrapper}>
      <img className={style.image} src={warn} alt="Warning" title="Попередження" />
    </div>
    <span className={style.text}>{children}</span>
  </div>
);

export default WarningText;
