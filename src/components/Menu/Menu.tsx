import { Link } from 'react-router-dom';
import { links } from 'constants/menuLinks';
import style from './Menu.module.scss';

const Menu = () => (
  <nav className={style.nav}>
    {links.map((item) => (
      <Link to={item.address} key={item.id} className={style.nav__link}>
        {item.name}
      </Link>
    ))}
  </nav>
);

export default Menu;
