export interface ICourtInfo {
    name: string;
    code: string;
    address?: string;
    email?: string;
    status?: string;
    occupation?: string;
}