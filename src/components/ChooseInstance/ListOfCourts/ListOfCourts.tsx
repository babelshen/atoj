import React, { useEffect, useState, Suspense, ChangeEvent } from 'react';
import CustomSearchInput from 'components/UI/CustomSearchInput';
import WarningText from 'components/WarningText';
import CustomLoading from 'components/UI/CustomLoading';
import style from './ListOfCourts.module.scss';
import { ICourtInfo } from '../interface';

const CardOfCourt = React.lazy(() => import('../CardOfCourt'));

const ListOfCourts: React.FC<{ listCourts: ICourtInfo[] }> = ({ listCourts }) => {
  const [filterListCourts, setFilterListCourts] = useState<ICourtInfo[]>([]);
  const [value, setValue] = useState<string>('');

  useEffect(() => {
    if (!value) {
      setFilterListCourts(listCourts);
    } else {
      const filterCourts = listCourts.filter((item) => item.name.toLowerCase().includes(value.toLowerCase()));
      setFilterListCourts(filterCourts as ICourtInfo[]);
    }
  }, [value, listCourts]);

  return (
    <>
      <CustomSearchInput type="text" placeholder="Укажіть назву суду" value={value} onChange={(e: ChangeEvent<HTMLInputElement>) => setValue(e.target.value)} onClick={() => setValue('')} />

      <WarningText>
        <p className={style.text}>Якщо Ви бажаєте надіслати повідомлення до суду електронною поштою, не забудьте використати електронний підпис перед відправленням листа.</p>
        <p>
          Відповідно до ч. 8 ст. 43 Цивільного процесуального кодексу України, якщо документи подаються учасниками справи до суду або надсилаються іншим учасникам справи в електронній формі, такі
          документи скріплюються електронним підписом учасника справи (його представника).
        </p>
      </WarningText>

      <div className={style.list_courts__wrapper}>
        {filterListCourts.map(item => (
          <div className={style.container} key={item.code}>
              <Suspense fallback={<CustomLoading />}>
                <CardOfCourt item={item as ICourtInfo} />
              </Suspense>
          </div>
        ))}
      </div>
    </>
  );
};

export default ListOfCourts;
