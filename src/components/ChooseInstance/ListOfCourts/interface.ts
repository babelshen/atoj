export interface ICourt {
  A: string | number; // Назва або id
  B: number | string; // Код або назва
  C: string | number; // Адреса або код
  D: string; // Email або адреса
  E?: string; // Email
}
