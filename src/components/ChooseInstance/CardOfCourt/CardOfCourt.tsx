import { memo } from 'react';
import { getUrlCourt } from 'config/courts/getUrlCourt';
import attention from '/attention.png';
import location from '/location.png';
import site from '/site.png';
import write from '/write.png';
import style from './CardOfCourt.module.scss';
import { ICourtInfo } from '../interface';

const CardOfCourt: React.FC<{ item: ICourtInfo }> = ({ item }) => {
  if (item.occupation) {
    return (
      <>
        <h2 className={style.court__title}>{item.name}</h2>
        <div className={style.status__wrapper}>
          <div className={style.status_image__wrapper}>
            <img src={attention} alt="Court status" title="Статус суду" className={style.status_image} />
          </div>
          <span className={style.status__text}>{item.occupation}</span>
        </div>
      </>
    );
  }
  if (item.status) {
    return (
    <>
    <h2 className={style.court__title}>{item.name}</h2>
    <div className={style.status__wrapper}>
          <div className={style.status_image__wrapper}>
            <img src={attention} alt="Court status" title="Статус суду" className={style.status_image} />
          </div>
          <span className={style.status__text}>{item.status}</span>
        </div>
        </>
  )}
  return (
    <>
      <h2 className={style.court__title}>{item.name}</h2>
      <div className={style.location__wrapper}>
        <div className={style.location__image_wrapper}>
          <img src={location} alt="Location" title="Адреса суду" className={style.location__image} />
        </div>
        <div className={style.location__text}>Розпорядник інформації приховав інформацію про адресу судів через введення військового стану</div>
      </div>

      <div className={style.buttons}>
        <a className={style.link} href={getUrlCourt(item.email as string)} target="_blank" rel="noreferrer">
          <div className={style.buttons__image_wrapper}>
            <img src={site} alt="Court web-site" title="Перейти на сайт суду" className={style.buttons__image} />
          </div>
        </a>
        <a className={style.link} href={`https://mail.google.com/mail/?view=cm&fs=1&to=${item.email}`} target="_blank" rel="noreferrer">
          <div className={style.buttons__image_wrapper}>
            <img src={write} alt="Court email" title="Надіслати повідомлення на електронну пошту суду" className={style.buttons__image} />
          </div>
        </a>
      </div>
    </>
  );
};

export default memo(CardOfCourt);
