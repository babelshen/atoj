import { memo } from 'react';
import * as con from 'constants/courtAddresses';
import { getListCourts } from 'config/courts/getListCourts';
import CustomButton from 'components/UI/CustomButton';
import { ISelectInstance } from './interface';
import style from './SelectInstance.module.scss';
import { ICourtInfo } from '../interface';

const SelectInstance: React.FC<ISelectInstance<ICourtInfo[]>> = ({ 
  setCommonCourt, 
  setInfo, 
  setListCourts, 
  setError 
}) => {
  const getCourts = async (url: string) => {
    try {
      setListCourts([]);
      const result = await getListCourts(url);
      setListCourts(result as ICourtInfo[]);
      setCommonCourt(false);
      setInfo(true);
      setError(false);
    } catch (err) {
      setError(true);
      setCommonCourt(false);
    }
  };

  return (
    <div className={style.wrapper}>
      <CustomButton
        onClick={() => {
          setCommonCourt(true);
          setInfo(false);
        }}
      >
        Місцеві загальні суди
      </CustomButton>
      <CustomButton onClick={() => getCourts(con.ADMINISTRATIVE_COURTS)}>Окружні адміністративні суди</CustomButton>
      <CustomButton onClick={() => getCourts(con.COMMERCIAL_COURTS)}>Окружні господарські суди</CustomButton>
      <CustomButton onClick={() => getCourts(con.APPEAL_COMMON_COURTS)}>Апеляційні суди</CustomButton>
      <CustomButton onClick={() => getCourts(con.APPEAL_ADMINISTRATIVE_COURTS)}>Апеляційні адміністративні суди</CustomButton>
      <CustomButton onClick={() => getCourts(con.APPEAL_COMMERCIAL_COURTS)}>Апеляційні господарські суди</CustomButton>
    </div>
  );
};

export default memo(SelectInstance);
