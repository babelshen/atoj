import * as con from 'constants/courtAddresses';
import { getListCourts } from 'config/courts/getListCourts';
import CustomButton from 'components/UI/CustomButton';
import { ISelectCommonCourts } from './interface';
import { ICourtInfo } from '../interface';
import style from './SelectCommonCourts.module.scss';

const SelectCommonCourts: React.FC<ISelectCommonCourts<ICourtInfo[]>> = ({ setCommonCourt, setInfo, setListCourts, setError }) => {
  const getCourts = async (url: string) => {
    try {
      const result = await getListCourts(url);
      setListCourts(result as ICourtInfo[]);
      setCommonCourt(false);
      setInfo(true);
      setError(false);
    } catch (err) {
      setError(true);
      setCommonCourt(false);
    }
  };
  return (
    <>
      <h2 className={style.title}>Оберіть регіон</h2>
      <div className={style.wrapper}>
        <CustomButton onClick={() => getCourts(con.COMMON_COURTS_KYIV)}>м. Київ</CustomButton>
        <CustomButton onClick={() => getCourts(con.COMMON_COURTS_SEVASTOPOL)}>м. Севастополь</CustomButton>
        <CustomButton onClick={() => getCourts(con.COMMON_COURTS_ARC)}>АP Крим</CustomButton>
        <CustomButton onClick={() => getCourts(con.COMMON_COURTS_VINNYTSKA)}>Вінницька область</CustomButton>
        <CustomButton onClick={() => getCourts(con.COMMON_COURTS_VOLYNSKA)}>Волинська область</CustomButton>
        <CustomButton onClick={() => getCourts(con.COMMON_COURTS_DNIPROPETROVSKA)}>Дніпропетровська область</CustomButton>
        <CustomButton onClick={() => getCourts(con.COMMON_COURTS_DONETSKA)}>Донецька область</CustomButton>
        <CustomButton onClick={() => getCourts(con.COMMON_COURTS_ZHYTOMYRSKA)}>Житомирська область</CustomButton>
        <CustomButton onClick={() => getCourts(con.COMMON_COURTS_ZAKARPATSKA)}>Закарпатська область</CustomButton>
        <CustomButton onClick={() => getCourts(con.COMMON_COURTS_ZAPORIZHZKA)}>Запорізька область</CustomButton>
        <CustomButton onClick={() => getCourts(con.COMMON_COURTS_IVANO_FRANKIVSKA)}>Івано-Франківська область</CustomButton>
        <CustomButton onClick={() => getCourts(con.COMMON_COURTS_KYIVSKA)}>Київська область</CustomButton>
        <CustomButton onClick={() => getCourts(con.COMMON_COURTS_KIROVOGRADSKA)}>Кіровоградська область</CustomButton>
        <CustomButton onClick={() => getCourts(con.COMMON_COURTS_LUHANSKA)}>Луганська область</CustomButton>
        <CustomButton onClick={() => getCourts(con.COMMON_COURTS_LVIVSKA)}>Львівська область</CustomButton>
        <CustomButton onClick={() => getCourts(con.COMMON_COURTS_MYKOLAIVSKA)}>Миколаївська область</CustomButton>
        <CustomButton onClick={() => getCourts(con.COMMON_COURTS_ODESKA)}>Одеська область</CustomButton>
        <CustomButton onClick={() => getCourts(con.COMMON_COURTS_POLTAVSKA)}>Полтавська область</CustomButton>
        <CustomButton onClick={() => getCourts(con.COMMON_COURTS_RIVNENSKA)}>Рівненська область</CustomButton>
        <CustomButton onClick={() => getCourts(con.COMMON_COURTS_SUMSKA)}>Сумська область</CustomButton>
        <CustomButton onClick={() => getCourts(con.COMMON_COURTS_TERNOPILSKA)}>Тернопільська область</CustomButton>
        <CustomButton onClick={() => getCourts(con.COMMON_COURTS_KHARKIVSKA)}>Харківська область</CustomButton>
        <CustomButton onClick={() => getCourts(con.COMMON_COURTS_KHERSONSKA)}>Херсонська область</CustomButton>
        <CustomButton onClick={() => getCourts(con.COMMON_COURTS_KHMELNITSKA)}>Хмельницька область</CustomButton>
        <CustomButton onClick={() => getCourts(con.COMMON_COURTS_CHERKASKA)}>Черкаська область</CustomButton>
        <CustomButton onClick={() => getCourts(con.COMMON_COURTS_CHERNIVETSKA)}>Чернівецька область</CustomButton>
        <CustomButton onClick={() => getCourts(con.COMMON_COURTS_CHERNIGIVSKA)}>Чернігівська область</CustomButton>
      </div>
    </>
  );
};

export default SelectCommonCourts;
