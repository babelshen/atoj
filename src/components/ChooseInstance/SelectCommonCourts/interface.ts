export interface ISelectCommonCourts<T> {
  setCommonCourt: (param: boolean) => void;
  setInfo: (param: boolean) => void;
  setListCourts: (param: T) => void;
  setError: (param: boolean) => void;
}
