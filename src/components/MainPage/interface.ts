import { RefObject } from 'react';

export interface IServicesRef {
  servicesRef: RefObject<HTMLDivElement> | null;
}
