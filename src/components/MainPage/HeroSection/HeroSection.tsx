import GeneralButton from 'components/UI/GeneralButton';
import { IServicesRef } from '../interface';
import style from './HeroSection.module.scss';

const HeroSection: React.FC<IServicesRef> = ({ servicesRef }) => {
  const handleScrollToServices = () => {
    if (servicesRef && servicesRef.current) servicesRef.current.scrollIntoView({ behavior: 'smooth' });
  };

  return (
    <section className={style.hero}>
      <h1 className={style.hero__title}>Access to Justice</h1>
      <span className={style.hero__description}>Зробимо спілкування з судовими органами України максимально зручним та зрозумілим</span>
      <GeneralButton type="button" onClick={handleScrollToServices}>
        Починаємо
      </GeneralButton>
    </section>
  );
};

export default HeroSection;
