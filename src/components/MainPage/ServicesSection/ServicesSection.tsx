import { mainServiceInfo } from 'constants/mainServiceInfo';
import CustomLink from 'components/UI/CustomLink';
import { IServicesRef } from '../interface';
import style from './ServicesSection.module.scss';

const ServicesSection: React.FC<IServicesRef> = ({ servicesRef }) => (
  <section className={style.services} ref={servicesRef}>
    <span className={style.services__title}>Пропонуємо Вам:</span>
    <div className={style.services__list}>
      {mainServiceInfo.map((item) => (
        <div key={item.id} className={style.services__item}>
          <div className={style.services__item_wrapper}>
            <img className={style.services__item__image} src={item.image} alt={item.name} title={item.name} />
          </div>
          <div className={style.services__item__data}>
            <h2 className={style.services__item__info_title}>{item.name}</h2>
            <p className={style.services__description}>{item.description}</p>
            <ul className={style.services__description}>
              {item.available
                ? item.available.map((itemCard, index) => (
                    <li key={index} className={style.available}>
                      {itemCard}
                    </li>
                  ))
                : item.inaccessible.map((itemCard, index) => (
                    <li key={index} className={style.inaccessible}>
                      {itemCard}
                    </li>
                  ))}
            </ul>
            {item.addInfo ? (
              <ul className={style.services__description}>
                <li className={style.description}>Через введення військового стану, розпорядник інформації обмежив до неї доступ</li>
              </ul>
            ) : (
              false
            )}
            {item.instruction ? (
              <CustomLink link={item.instruction}>Додаткова інформація</CustomLink>
            ): false}
            {item.button ? (
              <CustomLink link={item.link}>{item.button}</CustomLink>
            ) : (
              <CustomLink link={item.link} disabled>
                {item.buttonDisabled}
              </CustomLink>
            )}
          </div>
        </div>
      ))}
    </div>
  </section>
);

export default ServicesSection;
