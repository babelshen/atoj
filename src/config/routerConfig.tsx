import { Route, createBrowserRouter, createRoutesFromElements } from 'react-router-dom';
import ChooseInstancePage from 'pages/ChooseInstancePage';
import CreateStatementsPage from 'pages/CreateStatementsPage';
import ErrorPage from 'pages/ErrorPage';
import Layout from 'pages/Layout';
import MainPage from 'pages/MainPage';
import AboutUsPage from 'pages/AboutUsPage';
import ContactUsPage from 'pages/ContactUsPage';

const router = createBrowserRouter(
  createRoutesFromElements(
    <Route path="/">
      <Route
        index
        element={
          <Layout>
            <MainPage />
          </Layout>
        }
      />

      <Route
        path="about"
        element={
          <Layout>
            <AboutUsPage />
          </Layout>
        }
      />

      <Route
        path="courts"
        element={
          <Layout>
            <ChooseInstancePage />
          </Layout>
        }
      />

      <Route
        path="create"
        element={
          <Layout>
            <CreateStatementsPage />
          </Layout>
        }
      />

      <Route
        path="contact"
        element={
          <Layout>
            <ContactUsPage />
          </Layout>
        }
      />

      <Route
        path="*"
        element={
          <Layout>
            <ErrorPage />
          </Layout>
        }
      />
    </Route>,
  ),
);

export default router;
