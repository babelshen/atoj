export interface ICommonCourt {
    A: string; // Назва
    B: number; // Код
    C: string; // Адреса
    D: string; // Email або адреса
    E: string; // Статус
    F: string; // Підсудність
  }
  
  export interface ISpecialCourt {
    A: number; // id
    B: string; // назва
    C: number; // код
    D: string; // адреса
    E: string; // email
    F: string; // статус
    G: string; // підсудність
  }
  