export const getUrlCourt = (address: string | boolean) => {
  if (typeof address === 'string') {
    return `https://${address.slice(6)}`;
  }
};
