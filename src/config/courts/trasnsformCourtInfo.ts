import { ICommonCourt, ISpecialCourt } from "./interface";

export const transformCourtInfo = (data: ISpecialCourt[] | ICommonCourt[]) => {
    const result = data.map(item => {
        if (typeof item.A === 'number') {
            if (item.F) {
                return {
                    name: item.B,
                    code: item.C,
                    address: item.D,
                    email: item.E,
                    status: item.F,
                }
            } if ('G' in item) {
                return {
                    name: item.B,
                    code: item.C,
                    status: item.G,
                }
            }
            return {
                name: item.B,
                code: item.C,
                address: item.D,
                email: item.E,
            }
        }
        if (typeof item.A === 'string') {
            if (item.F) {
                return {
                    name: item.A,
                    code: item.B,
                    status: item.F,
                }
            } if (item.E) {
                return {
                    name: item.A,
                    code: item.B,
                    occupation: item.E,
                }
            }
            return {
                name: item.A,
                code: item.B,
                address: item.C,
                email: item.D
            }
        } 
        return new Error('Виникла помилка під час завантаження даних. Спробуйте пізніше знайти суд');
    });
    return result;
}