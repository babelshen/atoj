import { read, utils, WorkBook, WorkSheet } from 'xlsx';
import { transformCourtInfo } from './trasnsformCourtInfo';
import { ICommonCourt, ISpecialCourt } from './interface';

export const getListCourts = async (url: string) => {
  try {
    const res = await (await fetch(url)).arrayBuffer();
    const workBook: WorkBook = read(res);
    const workSheet: WorkSheet = workBook.Sheets[workBook.SheetNames[0]];
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const data: any = utils.sheet_to_json<ICommonCourt[] | ISpecialCourt[]>(workSheet).slice(1);
    const transformDate = transformCourtInfo(data);
    return transformDate;
  } catch (e) {
    throw new Error('Виникла помилка під час завантаження даних. Спробуйте пізніше знайти суд');
  }
};
