import React, { Suspense, useState } from 'react';
import SelectInstance from 'components/ChooseInstance/SelectInstance';
import SelectCommonCourts from 'components/ChooseInstance/SelectCommonCourts';
import CustomLoading from 'components/UI/CustomLoading';
import { ICourtInfo } from 'components/ChooseInstance/interface';
import style from './ChooseInstancePage.module.scss';

const ListOfCourts = React.lazy(() => import('components/ChooseInstance/ListOfCourts'));

const ChooseInstancePage: React.FC = () => {
  const [listCourts, setListCourts] = useState<ICourtInfo[]>([]);
  const [error, setError] = useState<boolean>(false);
  const [commonCourt, setCommonCourt] = useState<boolean>(false);
  const [info, setInfo] = useState<boolean>(false);

  return (
    <section className={style.wrapper}>
      <h2 className={style.title}>Оберіть інстанцію</h2>

      <SelectInstance setCommonCourt={setCommonCourt} setInfo={setInfo} setListCourts={setListCourts} setError={setError} />

      <div className={style.container}>{commonCourt && <SelectCommonCourts setCommonCourt={setCommonCourt} setInfo={setInfo} setListCourts={setListCourts} setError={setError} />}</div>

      {error ? (
        <div className={style.container__error}>
          <p className={style.error__message}>Щось трапилось під час завантаження даних</p>
          <p className={style.error__message}>Спробуйте пізніше</p>
        </div>
      ) : (
        <Suspense fallback={<CustomLoading />}>
          <div className={style.container}>{info && <ListOfCourts listCourts={listCourts} />}</div>
        </Suspense>
      )}
    </section>
  );
};

export default ChooseInstancePage;
