import { useState, useRef } from 'react';
import ModalWindow from 'components/CreateStatements/ModalWindow';
import Statement from 'components/CreateStatements/Statement/Statement';
import ChooseCaseCategory from 'components/CreateStatements/ChooseCaseCategory';
import ListOfStatements from 'components/CreateStatements/ListOfStatements';
import FormStatement from 'components/CreateStatements/FormStatement';
import ButtonStatement from 'components/CreateStatements/ButtonStatement';
import WarningText from 'components/WarningText';
import { ICreateStatements, IInfo } from 'components/CreateStatements/interface';
import style from './CreateStatementsPage.module.scss';

const CreateStatementsPage = () => {
  const [state, setState] = useState<null | ICreateStatements>(null);
  const [info, setInfo] = useState<null | IInfo>(null);
  const [category, setCategory] = useState('');

  const [firstStep, setFirstStep] = useState(true);
  const [secondStep, setSecondStep] = useState(false);

  const [sign, setSign] = useState('');
  const [openSign, setOpenSign] = useState(false);
  const canvasRef = useRef(null);

  const date = new Date();
  const dateNow = {
    year: date.getFullYear(),
    month: date.getMonth() + 1,
    day: date.getDate(),
  };
  return (
    <section>
      {info === null ? <h2 className={style.title}>Створити заяву | клопотання</h2> : <h2 className={style.title}>{info.title}</h2>}

      {firstStep ? <ChooseCaseCategory setCategory={setCategory} setFirstStep={setFirstStep} setSecondStep={setSecondStep} /> : false}

      {secondStep ? <ListOfStatements category={category} setCategory={setCategory} setInfo={setInfo} setFirstStep={setFirstStep} setSecondStep={setSecondStep} /> : false}

      {info ? <FormStatement setState={setState} info={info} setInfo={setInfo} setFirstStep={setFirstStep} setSecondStep={setSecondStep} /> : false}

      {state && info ? (
        <>
          <div className={style.statement}>
            <Statement state={state} info={info} dateNow={dateNow} sign={sign} />
          </div>

          <WarningText>
            <p>
              Функцію &quot;Додати підпис&quot; створено для оформлення заяв, які подаються до суду в електронній формі. Ця функція не звільняє Вас від обов&apos;язку додати електронний підпис
              відповідно до вимог ч. 8 ст. 43 Цивільного процесуального кодексу України та додана лише для покращення візуального вигляду Вашої заяви.
            </p>
            <p>Якщо Ви будете подавати заяву безпосередньо до суду у канцелярію або у судовому засіданні, поставте власноруч підпис під заявою або клопотанням.</p>
          </WarningText>

          <ButtonStatement state={state} sign={sign} setSign={setSign} setOpenSign={setOpenSign} />
        </>
      ) : (
        false
      )}

      {openSign ? <ModalWindow setOpenSign={setOpenSign} setSign={setSign} canvasRef={canvasRef} /> : false}
    </section>
  );
};

export default CreateStatementsPage;
