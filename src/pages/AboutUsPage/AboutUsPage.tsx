import { civilStatements } from 'constants/statements';
import { situationBlocks } from 'constants/aboutSituations';
import { useEffect } from 'react';
import style from './AboutUsPage.module.scss';
import court from '/court.png';
import statement from '/statement.png';

const AboutUsPage = () => {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);
  return(
    <section className={style.section__about}>
      <article className={style.general_info__wrapper}>
        <p>
          <span className={style.project}>AtoJ.</span> - це pet-project, спрямований на надання допомоги у комунікації з судовими органами влади України.
        </p>

        <p>
          <strong>Метою нашого проєкту</strong> є створення максимально зручного та зрозумілого ресурсу, який допоможе людям без юридичної освіти ефективно користуватися своїми правами для захисту
          інтересів у суді. Також наш ресурс допоможе відповідним спеціалістам пришвидшити виконання своїх професійних обов&apos;язків.
        </p>

        <p>
          Для реалізації цієї мети, ми просимо Вас залишати Ваші <strong>відгуки та пропозиції</strong>, аби ми мали змогу всебічно розвивати наш ресурс.
        </p>

        <p>
          Крім того, ми завжди <strong>відкриті до співпраці</strong>. Якщо у Вас є цікаві пропозиції - ми з радістю обговоримо їх з Вами.
        </p>

        <p>
          Ми не збираємо та не опрацьовуємо дані, які вказуються Вами для комунікації з судом. Тому Ви можете бути впевнені у <strong>збереженні приватності</strong>. На нашому ресурсі відповідальність
          за коректність та достовірність інформації несе користувач, який вказує інформацію.
        </p>

        <p>
          Хоча представники проєкту AtoJ. мають вищу юридичну освіту, однак ми не надаємо консультації з формування правової позиції у справі. Це виходить за межі мети проєкту. Будь-які рекомендації на
          нашому ресурсі мають <strong>рекомендаційних характер</strong>.
        </p>
      </article>

      <article className={style.situation}>
        <p className={style.situation__title}>Уявімо:</p>
        <div className={style.situation__container}>
          {situationBlocks.map((item) => (
            <div key={item.id} className={style.situation__container_item}>
              <div className={style.situation__container_image_wrapper}>
                <img className={style.situation__container_image} src={item.image} alt="Situation" />
              </div>
              <span className={style.situation__container_text}>{item.text}</span>
            </div>
          ))}
        </div>
      </article>

      <article className={style.capability}>
        <p className={style.capability__title}>Можливості:</p>
        <div className={style.capability__container}>
          <div className={style.capability__container_item}>
            <div className={style.capability__container_image_wrapper}>
              <img className={style.capability__container_image} src={court} alt="Situation" />
            </div>
            <p>
              Отримати <strong>актуальну інформацію</strong> про усі суди України першої та другої інстанцій.
            </p>
            <p>Перелік доступної інформації:</p>
            <ul className={style.capability__container_list}>
              <li className={style.capability__container_point}>Повне нейменування суду</li>
              <li className={style.capability__container_point}>Фактична адреса суду</li>
              <li className={style.capability__container_point}>Електронна адреса суду</li>
              <li className={style.capability__container_point}>Електронна пошта суду</li>
              <li className={style.capability__container_point}>Статус функціонування суду</li>
            </ul>
          </div>

          <div className={style.capability__container_item}>
            <div className={style.capability__container_image_wrapper}>
              <img className={style.capability__container_image} src={statement} alt="Situation" />
            </div>
            <p>
              <strong>Сгенерувати заяву або клопотання</strong> у справі, заповнивши просту форму.
            </p>
            <p>Кількість заяв та клопотань, доступних для створення на нашому ресурсі:</p>
            <ul className={style.capability__container_list}>
              <li className={style.capability__container_point}>
                Цивільні справи - <span className={style.project}>{civilStatements.length}</span>
              </li>
              <li className={style.capability__container_point}>
                Кримінальні справи - <span className={style.project}>0</span>
              </li>
              <li className={style.capability__container_point}>
                Справи про адміністративні правопорушення - <span className={style.project}>0</span>
              </li>
            </ul>
            <p>Уся інформація, яку Ви вказуєте, не є предметом аналізу та збереження на проєкті</p>
          </div>
        </div>
      </article>
    </section>
  );
}

export default AboutUsPage;
