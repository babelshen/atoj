export interface IContactUsPage {
    name: string;
    mail: string;
    message: string;
  }