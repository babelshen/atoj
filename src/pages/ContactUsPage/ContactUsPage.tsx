import CustomInput from "components/UI/CustomInput";
import { useForm } from "react-hook-form";
import GeneralButton from "components/UI/GeneralButton";
import CustomTextarea from "components/UI/CustomTextarea";
// eslint-disable-next-line import/no-extraneous-dependencies
import emailjs from '@emailjs/browser';
import { useEffect, useState } from "react";
import CustomModalWindow from "components/UI/CustomModalWindow";
import style from './ContactUsPage.module.scss';
import { IContactUsPage } from "./interface";

const ContactUsPage = () => {
  const [sendMesssage, setSendMessage] = useState<boolean>(false);
  const [errorSendMessage, setErrorSendMessage] = useState<boolean>(false);

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);
  
  const { register, handleSubmit, reset, formState: { errors } } = useForm<IContactUsPage>({mode: 'onBlur'});

  const onSubmit = (data: IContactUsPage) => {

    const serviceId = 'service_7ix6yfm';
    const templateId = 'template_712d3im';
    const publicKey = 'DbpKnRDOHe5clpInw';

    const templateParams = {
      from_name: data.name,
      from_email: data.mail,
      to_name: 'AtoJ.',
      message: data.message,
    };

    emailjs.send(serviceId, templateId, templateParams, publicKey)
      .then(response => { 
        if (response.status === 200) setSendMessage(prev => !prev);
        reset();
      })
      .catch(error => {
        console.log(error);
        setErrorSendMessage(prev => !prev)
      }) 
  } 

  const handleClick = () => {
    setSendMessage(false);
    setErrorSendMessage(false);
  }
  
  return(
    <>
    <form onSubmit={handleSubmit(onSubmit)} className={style.form}>
      <h1 className={style.contact__title}>Зв&apos;язатися з адміністрацією AtoJ.</h1>
      <CustomInput
        type="text"
        id="username"
        label="Зазначте Ваше ім'я"
        placeholder="Ім'я"
        error={errors.name}
        {...register('name', {
          required: "Це обов'язкове поле для заповнення",
          minLength: {
            value: 3,
            message: 'Це поле має містити не менше 3-х символів',
          },
          maxLength: {
            value: 15,
            message: 'Це поле має містити не більше 15-ти символів',
          },
          pattern: {
            value: /^[а-щА-ЩЬьЮюЯяЇїІіЄєҐґ'’`]+$/,
            message: 'Це поле має містити лише українські літери',
          },
        })}
      />

      <CustomInput
        type="email"
        id="usermail"
        label="Ваша електронна пошта"
        placeholder="E-mail"
        error={errors.mail}
        {...register('mail', {
          required: "Це обов'язкове поле для заповнення",
          pattern: {
            value: /^[a-zA-Z0-9]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/,
            message: 'Перевірте правильність написання електронної пошти',
          },
        })}
      />

      <CustomTextarea 
        rows={10}
        id="user_message"
        label="Ваше повідомлення"
        error={errors.message}
        {...register('message', {
          required: "Це обов'язкове поле для заповнення",
          minLength: {
            value: 3,
            message: 'Це поле має містити не менше 3-х символів',
          },
          maxLength: {
            value: 200,
            message: 'Це поле має містити не більше 200 символів',
          },
        })}
      />  
        
      <GeneralButton type='submit' onClick={handleSubmit(onSubmit)}>Надіслати</GeneralButton>
    </form>

    {sendMesssage ? <CustomModalWindow onClick={handleClick}>Повідомлення надіслано. Дякуємо за увагу!</CustomModalWindow> : false}
    {errorSendMessage ? <CustomModalWindow onClick={handleClick}>Сталася помилка. Спробуйте надіслати повідомлення пізніше.</CustomModalWindow> : false}
    </>
  )
}

export default ContactUsPage;