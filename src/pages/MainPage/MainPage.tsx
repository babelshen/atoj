import { useEffect, useRef } from 'react';
import HeroSection from 'components/MainPage/HeroSection';
import ServicesSection from 'components/MainPage/ServicesSection';

const MainPage: React.FC = () => {
  const servicesRef = useRef(null);

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);
  return (
    <>
      <HeroSection servicesRef={servicesRef} />
      <ServicesSection servicesRef={servicesRef} />
    </>
  );
};

export default MainPage;
