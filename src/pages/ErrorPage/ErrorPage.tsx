import { useEffect } from 'react';
import style from './ErrorPage.module.scss';

const ErrorPage = () => {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);
  return (
    <section className={style.error__container}>
      <div className={style.error__text}>Сторінку не знайдено</div>
      <div className={style.error__code}>
        <span className={style.error__first_number}>4</span>
        <span className={style.error__second_number}>0</span>
        <span className={style.error__third_number}>4</span>
      </div>
    </section>
  );
}

export default ErrorPage;
