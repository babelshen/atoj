import { RouterProvider } from 'react-router';
import router from 'config/routerConfig';

function App() {
  return <RouterProvider router={router} />;
}

export default App;
