export const projectLinks = [
  {
    id: 1,
    name: 'Новини',
    address: 'https://www.access-justice.net/f46-forum',
  },
  {
    id: 2,
    name: 'Про нас',
    address: '/about',
  },
  {
    id: 3,
    name: 'Напишіть нам',
    address: '/contact',
  },
  {
    id: 4,
    name: 'Співпраця',
    address: 'https://www.access-justice.net/t311-topic',
  },
  {
    id: 5,
    name: 'Підтримка',
    address: 'https://www.access-justice.net/f48-forum',
  },
  {
    id: 6,
    name: 'Пропозиції',
    address: 'https://www.access-justice.net/f49-forum',
  },
];

export const instructionsLinks = [
  {
    id: 1,
    name: 'Знайти суд',
    address: 'https://www.access-justice.net/t312-topic',
  },
  {
    id: 2,
    name: 'Створити клопотання',
    address: 'https://www.access-justice.net/t313-topic',
  },
];
