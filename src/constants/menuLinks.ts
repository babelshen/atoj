import { forumAddress } from './globalVariables';

export const links = [
  {
    id: 1,
    name: 'Головна',
    address: '/',
  },
  {
    id: 2,
    name: 'Про нас',
    address: '/about',
  },
  {
    id: 3,
    name: 'Інструкції',
    address: 'https://www.access-justice.net/f47-forum',
  },
  {
    id: 4,
    name: 'Спільнота',
    address: forumAddress,
  },
];
