export const caseCategoryButton = [
  {
    id: 1,
    name: 'Цивільна справа',
    category: 'Civil',
  },
  {
    id: 2,
    name: 'Кримінальна справа',
    category: 'Criminal',
  },
  {
    id: 3,
    name: 'Справа про адміністративне правопорушення',
    category: 'Offenses',
  },
];
