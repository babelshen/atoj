import court from '/court.png';
import statement from '/statement.png';
import judge from '/judges.png';

export const mainServiceInfo = [
  {
    id: 1,
    image: court,
    name: 'Судові інстанції',
    description: 'Дізнатися та використати:',
    available: ['фактичну адресу суду', 'електронну адресу суду', 'електронну пошту суду', 'статус функціонування суду'],
    button: 'Пошук інформації',
    link: '/courts',
    instruction: 'https://www.access-justice.net/t312-topic',
  },
  {
    id: 2,
    image: statement,
    name: 'Заява | клопотання',
    description: 'Створити звернення:',
    available: ['у цивільній справі', 'у кримінальній справі', 'у справі про адміністративне правопорушення'],
    button: 'Створити',
    link: '/create',
    instruction: 'https://www.access-justice.net/t313-topic',
  },
  {
    id: 3,
    image: judge,
    name: 'Судді України',
    description: 'Дізнатися:',
    inaccessible: ['контактну інформацію приймальні судді по справі'],
    addInfo: 'Через введення військового стану, розпорядник інформації обмежив до неї доступ',
    buttonDisabled: 'Дізнатися',
    link: '/judges',
    instruction: '',
  },
];
